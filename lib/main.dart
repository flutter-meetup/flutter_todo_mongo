import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:mongo_dart/mongo_dart.dart' show Db;
import 'package:provider/provider.dart';


/// Skeleton from the flutter create template with some additions:
/// * provider package used to separate the app state into NameListModel
/// * NameListModel persists to MongoDB using the mongo_dart package

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => NameListModel(),
      child: MyApp()
    )
  );
}

class NameListModel extends ChangeNotifier {
  // Our names listing
  List<dynamic> names = [];
  // How many we are storing (separated just for fun)
  int count = -1;

  // this mystical IP translates to your host computer...
  final _db = Db("mongodb://10.0.2.2:27017/flutter_testing");
  // the MongoCollection - this we need to initialize later as we need _db for it
  var _collection;

  NameListModel() {
    _connectToCollection();
  }

  void addName(String name) async {
    await _collection.insert({ "name": name });
    await _updateState();
    notifyListeners();
  }

  Future _connectToCollection() async {
    await _db.open();
    _collection = _db.collection("our_docs");
    await _updateState();
  }

  Future _updateState() async {
    var allDocs = await (_collection.find().toList());
    count = allDocs.length;
    names =  await _collection
        .find()
        .map((element) { return element["name"]; })
        .toList();
    notifyListeners();
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    void _addRandomName(names) {
      var randomNames = ["George", "Jose", "Mary", "Joseph", "Dingo", "Lucy"];
      names.addName((randomNames..shuffle()).first);
    }

    return Consumer<NameListModel>(
      builder: (context, namesList, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Flutter Mongo Demo"),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'There are ${namesList.count} documents in our Mongo Collection',
                ),
                namesList.count > 0 ?
                  ListView.builder(
                    padding: EdgeInsets.all(4),
                    shrinkWrap: true,
                    itemCount: namesList.count,
                    itemBuilder: (context, index) {
                      return ListTile(
                          title: Center(
                            child: Text(namesList.names[index])
                          )
                      );
                    }
                  )
                  :
                  Text("No names available yet")
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => _addRandomName(namesList),
            tooltip: 'Increment',
            child: Icon(Icons.add),
          ),
        );
      }
    );
  }
}
